%-----------------------------------------------------------------------
% Job saved on 30-Apr-2023 19:37:57 by cfg_util (rev $Rev: 7345 $)
% spm SPM - SPM12 (7771)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
matlabbatch{1}.spm.dcm.spec.meeg.D = {'C:\Users\truio\tn-project\Data\anta\DPRST_0101\mDPRST_0101_MMN_preproc.mat'};
matlabbatch{1}.spm.dcm.spec.meeg.dcmmat = {'C:\Users\truio\tn-project\DCM_mDPRST_0101_MMN_preproc.mat'};
matlabbatch{1}.spm.dcm.spec.meeg.pE = {''};
matlabbatch{1}.spm.dcm.spec.meeg.P = {''};
matlabbatch{1}.spm.dcm.spec.meeg.feedback = 1;
matlabbatch{1}.spm.dcm.spec.meeg.output = 'GCM';
matlabbatch{1}.spm.dcm.spec.meeg.dir = {'C:\Users\truio\tn-project\DCM'};
