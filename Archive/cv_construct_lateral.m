function A3 = cv_construct_lateral()
% Constructs lateral connections matrices
% FORMAT A3 = cv_construct_lateral()
%
% A3        - Cell array of lateral connection matrices
%__________________________________________________________________________
% 
% cv_construct_lateral constructs a cell array of matrices representing the
% lateral connections of the reduced DCMs.
%__________________________________________________________________________

A3 = cell(23, 1);
Nareas = 5;

for i = 1:23
    A3{i} = zeros(Nareas, Nareas);
    % Model 1-15: Lateral connections, Model 16-23: No lateral connections
    if i <= 15
        A3{i}(4,3) = 1;
        A3{i}(3,4) = 1;
    end
end

end