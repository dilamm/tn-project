function [A3, A3_C] = cv_construct_lateral_prior()
% Constructs lateral connections matrices
% FORMAT A3 = cv_construct_lateral_prior()
%
% A3        - Cell array of priors for lateral connections
%__________________________________________________________________________
% 
% cv_construct_lateral_prior constructs a cell array of matrices representing the
% lateral connections of the reduced DCMs.
%__________________________________________________________________________

A3 = cell(23, 1);
A3_C = cell(23, 1);
Nareas = 5;

for i = 1:23
    A3{i} = -32*ones(Nareas, Nareas);
    % Model 1-15: Lateral connections, Model 16-23: No lateral connections
    if i <= 15
        A3{i}(4,3) = 0;
        A3_C{i}(4,3) = 0.125;
        A3{i}(3,4) = 0;
        A3_C{i}(3,4) = 0.125;
    end
end

end