function B = cv_construct_modulation()
% Constructs modulation matrices
% FORMAT B = cv_construct_modulation()
%
% B        - Cell array of modulation matrices
%__________________________________________________________________________
% 
% cv_construct_modulation constructs a cell array of matrices representing 
% the modulations of the connections in the reduced DCMs.
%__________________________________________________________________________


B = cell(23, 1);
Nareas = 5;

forward = zeros(Nareas,Nareas);
forward(3,1) = 1;
forward(4,2) = 1;
forward(5,4) = 1;

backward = zeros(Nareas,Nareas);
backward(1,3) = 1;
backward(2,4) = 1;
backward(4,5) = 1;

lateral = zeros(Nareas,Nareas);
lateral(4,3) = 1;
lateral(3,4) = 1;

intrinsic = zeros(Nareas,Nareas);
intrinsic(1,1) = 1;
intrinsic(2,2) = 1;

% Model 1-15: Lateral connections

% Model 1: forward, backward, lateral modulation
B{1} = forward + backward + lateral;

% Model 2: forward, backward, intrinsic modulation
B{2} = forward + backward + intrinsic;

% Model 3: forward, backward modulation
B{3} = forward + backward;

% Model 4: forward, lateral, intrinsic modulation
B{4} = forward + lateral + intrinsic;

% Model 5: forward, lateral modulation
B{5} = forward + lateral;

% Model 6: backward, lateral, intrinsic modulation
B{6} = backward + lateral + intrinsic;

% Model 7: backward, lateral modulation
B{7} = backward + lateral;

% Model 8: forward intrinsic modulation
B{8} = forward + intrinsic;

% Model 9: forward modulation
B{9} = forward;

% Model 10: backward, intrinsic modulation
B{10} = backward + intrinsic;

% Model 11: backward modulation
B{11} = backward;

% Model 12: lateral, intrinsic modulation
B{12} = lateral + intrinsic;

% Model 13: lateral modulation
B{13} = lateral;

% Model 14: intrinsic modulation
B{14} = intrinsic;

% Model 15: no modulation
B{15} = zeros(Nareas, Nareas);

% Model 16-23: No lateral connections

% Model 16: forward, backward, intrinsic modulation
B{16} = forward + backward + intrinsic;

% Model 17: forward, backward modulation
B{17} = forward + backward;

% Model 18: forward, intrinsic modulation
B{18} = forward + intrinsic;

% Model 19: forward modulation
B{19} = forward;

% Model 20: backward, intrinsic modulation
B{20} = backward + intrinsic;

% Model 21: backward modulation
B{21} = backward;

% Model 22: intrinsic modulation
B{22} = intrinsic;

% Model 23: no modulation
B{23} = zeros(Nareas, Nareas);

end