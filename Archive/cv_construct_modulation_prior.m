function B = cv_construct_modulation_prior()
% Constructs modulation matrices
% FORMAT B = cv_construct_modulation_prior()
%
% B        - Cell array of modulation priors
%__________________________________________________________________________
% 
% cv_construct_modulation_prior constructs a cell array of matrices 
% representing the modulations of the connections in the reduced DCMs.
%__________________________________________________________________________


B = cell(23, 1);
Nareas = 5;

forward = zeros(Nareas,Nareas);
forward(3,1) = -32;
forward(4,2) = -32;
forward(5,4) = -32;

backward = zeros(Nareas,Nareas);
backward(1,3) = -32;
backward(2,4) = -32;
backward(4,5) = -32;

lateral = zeros(Nareas,Nareas);
lateral(4,3) = -32;
lateral(3,4) = -32;

intrinsic = zeros(Nareas,Nareas);
intrinsic(1,1) = -32;
intrinsic(2,2) = -32;

% Model 1-15: Lateral connections

% Model 1: forward, backward, lateral modulation: intrinsic off
B{1} = intrinsic;

% Model 2: forward, backward, intrinsic modulation: lateral off
B{2} = lateral;

% Model 3: forward, backward modulation: lateral, intrinsic off
B{3} = lateral + intrinsic;

% Model 4: forward, lateral, intrinsic modulation: backward off
B{4} = backward;

% Model 5: forward, lateral modulation: backward, intrinsic off
B{5} = backward + intrinsic;

% Model 6: backward, lateral, intrinsic modulation: forward off
B{6} = forward;

% Model 7: backward, lateral modulation: forward, intrinsic off
B{7} = forward + intrinsic;

% Model 8: forward, intrinsic modulation: backward, lateral off
B{8} = backward + lateral;

% Model 9: forward modulation: backward, lateral, intrinsic off
B{9} = backward + lateral + intrinsic;

% Model 10: backward, intrinsic modulation: forward, lateral off
B{10} = forward + lateral;

% Model 11: backward modulation: forward, lateral, intrinsic off
B{10} = forward + lateral + intrinsic;

% Model 12: lateral, intrinsic modulation: forward, backward off
B{12} = forward + backward;

% Model 13: lateral modulation: forward, backward, intrinsic off
B{13} = forward + backward + intrinsic;

% Model 14: intrinsic modulation: forward, backward, lateral off
B{14} = forward + backward + lateral;

% Model 15: no modulation: all off
B{15} = forward + backward + lateral + intrinsic;

% Model 16-23: No lateral connections

% Model 16: forward, backward, intrinsic modulation: no off
B{16} = zeros(Nareas, Nareas);

% Model 17: forward, backward modulation: intrinsic off
B{17} = intrinsic;

% Model 18: forward, intrinsic modulation: backward off
B{18} = backward;

% Model 19: forward modulation: backward, intrinsic off
B{19} = backward + intrinsic;

% Model 20: backward, intrinsic modulation: forward off
B{20} = forward;

% Model 21: backward modulation: forward, intrinsic off
B{21} = forward + intrinsic;

% Model 22: intrinsic modulation: forward, backward off
B{22} = forward + backward;

% Model 23: no modulation: all off
B{23} = forward + backward + intrinsic;

end