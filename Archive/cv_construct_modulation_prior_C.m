function B_C = cv_construct_modulation_prior_C()
% Constructs modulation matrices
% FORMAT B_C = cv_construct_modulation_prior_C()
%
% B_C       - Cell array of prior covariances for modulation matrices
%__________________________________________________________________________
% 
% cv_construct_modulation_prior_C constructs a cell array of matrices representing 
% the modulations of the connections in the reduced DCMs.
%__________________________________________________________________________


B_C = cell(23, 1);
Nareas = 5;

forward = zeros(Nareas,Nareas);
forward(3,1) = 0.125;
forward(4,2) = 0.125;
forward(5,4) = 0.125;

backward = zeros(Nareas,Nareas);
backward(1,3) = 0.125;
backward(2,4) = 0.125;
backward(4,5) = 0.125;

lateral = zeros(Nareas,Nareas);
lateral(4,3) = 0.125;
lateral(3,4) = 0.125;

intrinsic = zeros(Nareas,Nareas);
intrinsic(1,1) = 0.125;
intrinsic(2,2) = 0.125;

% Model 1-15: Lateral connections

% Model 1: forward, backward, lateral modulation
B_C{1} = forward + backward + lateral;

% Model 2: forward, backward, intrinsic modulation
B_C{2} = forward + backward + intrinsic;

% Model 3: forward, backward modulation
B_C{3} = forward + backward;

% Model 4: forward, lateral, intrinsic modulation
B_C{4} = forward + lateral + intrinsic;

% Model 5: forward, lateral modulation
B_C{5} = forward + lateral;

% Model 6: backward, lateral, intrinsic modulation
B_C{6} = backward + lateral + intrinsic;

% Model 7: backward, lateral modulation
B_C{7} = backward + lateral;

% Model 8: forward intrinsic modulation
B_C{8} = forward + intrinsic;

% Model 9: forward modulation
B_C{9} = forward;

% Model 10: backward, intrinsic modulation
B_C{10} = backward + intrinsic;

% Model 11: backward modulation
B_C{11} = backward;

% Model 12: lateral, intrinsic modulation
B_C{12} = lateral + intrinsic;

% Model 13: lateral modulation
B_C{13} = lateral;

% Model 14: intrinsic modulation
B_C{14} = intrinsic;

% Model 15: no modulation
B_C{15} = zeros(Nareas, Nareas);

% Model 16-23: No lateral connections

% Model 16: forward, backward, intrinsic modulation
B_C{16} = forward + backward + intrinsic;

% Model 17: forward, backward modulation
B_C{17} = forward + backward;

% Model 18: forward, intrinsic modulation
B_C{18} = forward + intrinsic;

% Model 19: forward modulation
B_C{19} = forward;

% Model 20: backward, intrinsic modulation
B_C{20} = backward + intrinsic;

% Model 21: backward modulation
B_C{21} = backward;

% Model 22: intrinsic modulation
B_C{22} = intrinsic;

% Model 23: no modulation
B_C{23} = zeros(Nareas, Nareas);

end