function P = cv_construction(full_DCM, D, ind_subjects)
% Constructs DCM array for BMR
% FORMAT P = cv_construction(full_DCM, D, ind_subjects)
% 
% DCM_full      - Array containing the full DCM inverted for all subjects
% D             - Array containing the averaged MEEG object for all
%               subjects
% ind_subjects  - Indices of subjects
%
% P             - {Nsub x Nmodel} cell array of DCM model structures of 
%               Nsub subjects, where each model is reduced independently
%__________________________________________________________________________
% cv_construction creates a cell array of DCM files of Nsub subjects. The
% first column of the array contains the full DCM, already inverted. The
% next rows contain reduced models, defined by the lateral connection
% matrix A3, and the modulation matrix B.
%__________________________________________________________________________

Nsub = numel(ind_subjects); % Number of subjects in training set
Nmodel = 24; % Number of models
P = cell(Nsub, Nmodel); % Initialize

% Cells containing the A3 and B matrices for the reduced models
[A3, A3_C] = cv_construct_lateral_prior(); %ADAPTED
B = cv_construct_modulation_prior(); %ADAPTED
B_C = cv_construct_modulation_prior_C(); %ADAPTED

k = 1; 
for i = ind_subjects
    % For the first DCM model: DCM_full
    P{k, 1} = full_DCM{i}; 
    %P{k, 1} = rmfield(P{k, 1},'M'); %NEW
    %P{k, 1}.M.dipfit = spm_dcm_erp_dipfit(P{k, 1}); %NEW
    for j = 2:Nmodel
        % Output for subject i, model j-1
        P{k,j} = cv_reduced_dcm(full_DCM{i}, A3{j-1}, B{j-1}, j-1); %NEW
        %P{k,j} = cv_reduced_dcm_prior(full_DCM{i}, A3{j-1}, A3_C{j-1}, B{j-1}, B_C{j-1}, j-1); %ADAPTED
        %P{k,j} = cv_reduced_dcm_old(D{i}, full_DCM{i}, A3{j-1}, B{j-1}, j-1); %NEW

    end
    k = k + 1; 
end
end