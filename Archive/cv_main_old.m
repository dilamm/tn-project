function cv_main_old(output_DCM, output_D)
% Perform the cross validation
% FORMAT cv_main(output_DCM, output_D)
%
% output_DCM        - Cell array of full DCM structures for each subject
% output_D          - Cell array of MEEG objects for each subject
%__________________________________________________________________________
% 
% cv_main performs the cross validation using the k-fold method for k = 7.
% For each training set, per drug group, the DCM best representing that
% drug group is selected using first BMR, than PEB, and finally BMS. For
% the test set, the classification is performed based on the DCMs
% representing the drug groups: each representing DCM is inverted on the
% test subject, and the subject is classified according to the DCM that
% explained the test subject's data best.
%
% Each subject is identified by the index corresponding to the subject in 
% "Data\pharma\dprst_anta_druglabels.csv", rather than by the primary
% identifying code as in the name of the MEEG objects.
%__________________________________________________________________________

% Reading csv file with drug label corresponding to subjects: 
% Biperiden = 1, Amisulpride = 2, Placebo = 3
drug_labels = readmatrix("Data\pharma\dprst_anta_druglabels.csv");
predictions = zeros(71, 1);

% Split data
cv_dcm = cvpartition(71,"KFold", 7);

% For each partition
for i = 1:7
    ind_train = training(cv_dcm, i); % Logical array
    ind_test = test(cv_dcm, i); % Logical array

    % TRAINING
    best_DCM = cell(3, 1); % Initialize
    % For each group of drug
    for drug = 1:3
        % Define groups within training set
        ind_subject = cv_group_sort(drug, ind_train, drug_labels);

        % Bayesian Model Reduction
        %P = cv_construction(output_DCM, output_D, ind_subject);
        P = cv_construction_only_full(output_DCM, output_D, ind_subject); % ONLY FULL MODEL
        %[RCM, BMC, BMA] = spm_dcm_bmr(P); % INCLUDED IN PEB?
        
        % RCM = cv_convert_to_struct(RCM, BMC);
    
        % Parametric Empirical Bayes, without group effects
        [~, PEB] = spm_dcm_peb(P);

        
        % Bayesian Model Selection
        [~, exp_r, ~, pxp, ~, ~] = spm_dcm_bmc(P);
        % Select DCM that best represents drug group
        [best_DCM_drug, max_exp_r, max_pxp] = cv_bms(exp_r, pxp, P);
        disp("Expected posterior probability: ")
        disp(max_exp_r)
        disp("Protected exceedance probability")
        disp(max_pxp)

        best_DCM{drug} = best_DCM_drug;
    end

    % TEST    
    % Convert logical array to array of indices
    test_set = 1:71;
    test_set = test_set(ind_test);
    for subject = test_set
        % Invert DCMs representing drug groups on test subject
        F = cv_inversion_test(subject, output_D{subject}, best_DCM);
        % Select best fitting DCM
        [max_F, argmax_F] = max(F);
        % Classification based on DCM representing drug groups
        predictions(subject) = argmax_F;
    end

end

[accuracy, C] = cv_compute_accuracy(drug_labels, predictions); % DEVELOP
% Total accuracy, accuracy per drug (true and predicted) --> TABLE

end