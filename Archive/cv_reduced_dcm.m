function reduced_DCM = cv_reduced_dcm(full_DCM, A3, B, number)
% Constructs reduced DCM
% FORMAT reduced_DCM = cv_reduced_dcm(full_DCM, A3, B, number)
% 
% full_DCM  - Full DCM structure
% A3        - Matrix representing lateral connections
% B         - Matrix representing modulations
% number    - Number identifying the reduced model
%
% reduced_DCM       - Reduced DCM structure
%__________________________________________________________________________
% cv_reduced_dcm returns the reduced DCM model structure defined by the 
% lateral connection matrix A3, and the modulation matrix B for the subject
% for which the full DCM is given.
%__________________________________________________________________________

% Copy full DCM
reduced_DCM = full_DCM;
%reduced_DCM = rmfield(reduced_DCM,'M'); %NEW
%reduced_DCM = spm_dcm_erp_dipfit(reduced_DCM); %NEW
% Reduce connections and modulations
reduced_DCM.A{3} = A3;
reduced_DCM.B{1} = B;
% Set name
reduced_DCM.name = char(full_DCM.name + "_reduced" + string(number));

end