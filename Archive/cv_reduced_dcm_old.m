function DCM = cv_reduced_dcm_old(D, full_DCM, A3, B, number)
% Constructs reduced DCM
% FORMAT DCM = cv_reduced_dcm(D, A3, B)
% 
% D         - MEEG object
% A3        - Matrix representing lateral connections
% B         - Matrix representing modulations
%
% DCM       - Reduced DCM 
%__________________________________________________________________________
%
% cv_reduced_dcm returns the reduced DCM model structure defined by the 
% lateral connection matrix A3, and the modulation matrix B for the subject
% defined by its MEEG object D.
%__________________________________________________________________________

% Data filename
DCM.xY.Dfile = D.fullfile;

% Parameters and options used for setting up model
DCM.options.analysis = 'ERP'; % analyze evoked responses
DCM.options.model    = 'NMDA'; % ERP model
DCM.options.spatial  = 'ECD'; % spatial model
DCM.options.trials   = [2 3]; % index of ERPs within ERP/ERF file
DCM.options.Tdcm(1)  = 0;     % start of peri-stimulus time to be modelled
DCM.options.Tdcm(2)  = 300;   % end of peri-stimulus time to be modelled
DCM.options.Nmodes   = 8;     % nr of modes for data selection
DCM.options.h        = 1;     % nr of DCT components
DCM.options.onset    = 60;    % selection of onset (prior mean)
DCM.options.D        = 1;     % downsampling

% Data and spatial model
DCM = spm_dcm_erp_data(DCM);

% Location priors for dipoles
DCM.Lpos  = [[-42; -22; 7] [46; -14; 8] [-61; -32; 8] [59; -25; 8] [46; 20; 8]];
DCM.Sname = {'left A1', 'right A1', 'left STG', 'right STG', 'right IFG'};
Nareas    = size(DCM.Lpos,2);

% Spatial model
DCM = spm_dcm_erp_dipfit(DCM);

DCM.M.dipfit.Lpos

% Specify connectivity model
DCM.A{1} = zeros(Nareas,Nareas);
DCM.A{1}(3,1) = 1;
DCM.A{1}(4,2) = 1;
DCM.A{1}(5,4) = 1;

DCM.A{2} = zeros(Nareas,Nareas);
DCM.A{2}(1,3) = 1;
DCM.A{2}(2,4) = 1;
DCM.A{2}(4,5) = 1;

DCM.A{3} = A3;

DCM.B{1} = B; 

DCM.C = [1; 1; 0; 0; 0];

% Between trial effects
DCM.xU.X = [0; 1];
DCM.xU.name = {'deviant'};

DCM.name = char(full_DCM.name + "_reduced" + string(number));

end 