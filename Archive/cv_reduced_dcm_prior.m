function reduced_DCM = cv_reduced_dcm_prior(full_DCM, A3, A3_C, B, B_C, number)
% Constructs reduced DCM
% FORMAT reduced_DCM = cv_reduced_dcm_prior(full_DCM, A3, A3_C, B, B_C, number)
% 
% full_DCM  - Full DCM structure
% A3        - Matrix representing lateral connections
% A3_C
% B         - Matrix representing modulations
% B_C
% number    - Number identifying the reduced model
%
% reduced_DCM       - Reduced DCM structure
%__________________________________________________________________________
% cv_reduced_dcm returns the reduced DCM model structure defined by the 
% lateral connection matrix A3, and the modulation matrix B for the subject
% for which the full DCM is given.
%__________________________________________________________________________

% Copy full DCM
reduced_DCM = full_DCM;
%reduced_DCM = rmfield(reduced_DCM,'M'); %NEW
%reduced_DCM = spm_dcm_erp_dipfit(reduced_DCM); %NEW
% Reduce connections and modulations
reduced_DCM.M.pE.A{3} = A3;
reduced_DCM.M.pC.A{3} = A3_C;
reduced_DCM.M.pE.B{1} = B;
reduced_DCM.M.pC.B{1} = B_C;
% Set name
reduced_DCM.name = char(full_DCM.name + "_reduced" + string(number));

end