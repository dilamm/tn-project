function D_eeg = dcm_eeg_channel_selection(D)
% Select EEG channels
% FORMAT D_eeg = dcm_eeg_channel_selection(D)
% 
% D             - MEEG object
% D_eeg         - MEEG object containing only EEG channels
%__________________________________________________________________________
% 
% dcm_eeg_channels creates an MEEG object containing only the EEG channels.
% The MEEG object is automatically saved in the same folder as the original
% MEEG object, with the prefix '_eeg'.
%__________________________________________________________________________

% Get indices of EEG channels
eeg_chan_idx = D.indchantype('EEG');
n = numel(eeg_chan_idx);
% Clone the original MEEG object with the number of channels equal to the
% number of EEG channels
D_eeg = clone(D, ['_eeg' fname(D)], [n D.nsamples D.ntrials]);
% Set the data of the new MEEG object 
D_eeg(:, :, :) = D(eeg_chan_idx, :, :);

%D_eeg = fiducials(D_eeg, D.fiducials);
D_eeg = chantype(D_eeg, 1:n, 'EEG');
D_eeg = units(D_eeg, 1:n, 'V');
%D_eeg = sensors(D_eeg, 'EEG', D.sensors('EEG'));
%D_eeg.sensors('EEG')
end