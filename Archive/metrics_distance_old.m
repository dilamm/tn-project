function dist = metrics_distance_old(fold, field)
% Computes the Mahalanobis distance between posterior parameter estimates
% FORMAT dist = metrics_distance(fold, field)
%
% fold             - Fold for which to calculate
% field            - Field identifying posterior parameter estimates
%
% dist             - Cell array of Mahalanobis distance from the elements
%                    dist{1}: of Drug2 to Drug1
%                    dist{2}: of Drug3 to Drug1
%                    dist{3}: of Drug1 to Drug2
%                    dist{4}: of Drug3 to Drug2
%                    dist{5}: of Drug1 to Drug3
%                    dist{6}: of Drug2 to Drug3
%__________________________________________________________________________
% metrics_distance computes the Mahalanobis distance from the posterior
% parameter estimates in the field identified by this variable of the DCM
% representing each drug group, to the population of posterior parameters
% of this field of the DCMs representing every other drug group.
%__________________________________________________________________________

% Load PEB results for each drug
P_PEB1 = load("Data/P_PEB/P_PEB_fold" + string(fold) + "_drug1", "P_PEB");
P_PEB2 = load("Data/P_PEB/P_PEB_fold" + string(fold) + "_drug2", "P_PEB");
P_PEB3 = load("Data/P_PEB/P_PEB_fold" + string(fold) + "_drug3", "P_PEB");

ind = load("Data/ind_res_DCM", "ind_res_DCM");
ind = ind.ind_res_DCM(fold, :);

% Get number of subjects per group
n1 = numel(P_PEB1.P_PEB{:, ind(1)});
n2 = numel(P_PEB2.P_PEB{:, ind(2)});
n3 = numel(P_PEB3.P_PEB{:, ind(3)});

% Number of parameters to compare
param = getfield(P_PEB1.P_PEB{1, ind(1)}.Ep, field);
if iscell(param)
    param = cell2mat(param);
end
n_param = numel(param);

% Place parameters in new format
X1 = zeros(n1, n_param);
for i = 1:n1
    param = getfield(P_PEB1.P_PEB{i, ind(1)}.Ep, field);
    if iscell(param)
        param = cell2mat(param);
    end
    X1(i, :) = reshape(param, [1, n_param]);
end

X2 = zeros(n2, n_param);
for i = 1:n2
    param = getfield(P_PEB2.P_PEB{i, ind(2)}.Ep, field);
    if iscell(param)
        param = cell2mat(param);
    end
    X2(i, :) = reshape(param, [1, n_param]);
end

X3 = zeros(n3, n_param);
for i = 1:n3
    param = getfield(P_PEB3.P_PEB{i, ind(3)}.Ep, field);
    if iscell(param)
        param = cell2mat(param);
    end
    X3(i, :) = reshape(param, [1, n_param]);
end

% Compute Mahalanobis distance
dist = cell(6, 1);

dist{1} = mahal(X2, X1);
dist{2} = mahal(X3, X1);
dist{3} = mahal(X1, X2);
dist{4} = mahal(X3, X2);
dist{5} = mahal(X1, X3);
dist{6} = mahal(X2, X3);


end