function F = metrics_inversion(subject, D_sim, res_DCM)
%
%__________________________________________________________________________
%
%__________________________________________________________________________

F = zeros(3, 1);

for drug = 1:3
    % Get DCM
    DCM = res_DCM{drug};
    % Remove subject specific fields
    DCM = rmfield(DCM, {'xY', 'name', 'Ep', 'Cp', 'Eg', 'Cg', 'Ce', 'Pp', 'H', 'K', 'x', 'R', 'F', 'L', 'ID'});
    % Set data from subject
    DCM.xY = D_sim.fullfile;
    % Invert
    DCM.name = char('Data\Identifiability\DCM\DCM_res_subj' + string(subject) + '_drug_' + string(drug));
    DCM = spm_dcm_erp(DCM);
    % Get free energy
    F(drug) = DCM.F;
end

end