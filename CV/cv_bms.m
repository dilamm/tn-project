function [best_DCM, ind, max_exp_r, max_pxp] = cv_bms(exp_r, pxp, P)
% Select the DCM based on the expected posterior probability and the 
% protected exceedance probability.
% FORMAT [best_DCM, ind, max_exp_r, max_pxp] = cv_bms(exp_r, pxp, P)
%
% exp_r         - M x 1 array of expected posterior probability
% pxp           - M x 1 array of protected exceedance probability
% P             - N x M cell array of DCM structures, with N the number of
%                 subjects and M the number of models
%
% best_DCM      - DCM structure with the highest exp_r and pxp
% ind           - index of best_DCM
% max_exp_r     - Highest exp_r
% max_pxp       - Highest pxp
%__________________________________________________________________________
% 
% cv_bms selects the DCM structure with the highest expected posterior 
% probability and the highest protected exceedance probability. If these
% measures do not select the same structure, a message is printed.
%__________________________________________________________________________

% Get maximal exp_r and pxp
[max_exp_r, argmax_exp_r] = max(exp_r);
[max_pxp, argmax_pxp] = max(pxp);

% If measures selects different DCM structures, print failure message
if argmax_exp_r ~= argmax_pxp
    disp("BMS failed: multiple models selected.")
end

% Select DCM
ind = argmax_exp_r;
best_DCM = P{1, argmax_exp_r};

end