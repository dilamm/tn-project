function P_PEB = cv_concatenate(P_PEB_full, P_PEB_no_lat, P_PEB_no_mod)
% Concatenate cells.
% FORMAT P_PEB = cv_concatenate(P_PEB_full, P_PEB_no_lat, P_PEB_no_mod)
%
% P_PEB_full   - Cell array of full DCM structures after PEB
% P_PEB_no_lat - Cell array of DCM structures without lateral 
%                     connections after PEB
% P_PEB_no_mod - Cell array of DCM structures without modulation on 
%                     the lateral connections after PEB
% P_PEB        - Concatenated cell array
%__________________________________________________________________________
% 
% cv_concatenate concatenates 3 cell array of dimension N x 1 to form a
% cell array of dimension N x 3.
%__________________________________________________________________________

n = numel(P_PEB_full);
P_PEB = cell(n, 3);

for i = 1:n
    P_PEB{i, 1} = P_PEB_full{i};
    P_PEB{i, 2} = P_PEB_no_lat{i};
    P_PEB{i, 3} = P_PEB_no_mod{i};
end

end