function P = cv_construction(DCM, ind_subjects)
% Construct DCM array for PEB.
% FORMAT P = cv_construction(DCM, ind_subjects)
% 
% DCM           - Array containing the DCM inverted for all subjects
% ind_subjects  - Indices of subjects
%
% P             - {Nsub x 1} cell array of DCM structures of Nsub subjects
%__________________________________________________________________________
% 
% cv_construction creates a cell array of DCM structures of Nsub subjects.
% The DCM structures are already inverted. 
%__________________________________________________________________________

Nsub = numel(ind_subjects); % Number of subjects in training set
P = cell(Nsub, 1); % Initialize

k = 1; 
for i = ind_subjects
    P{k, 1} = DCM{i}; 
    k = k + 1; 
end
end