function ind_subject = cv_group_sort(drug, ind_train, drug_labels)
% Select subjects belonging to the group 'drug' within the training set.
% FORMAT ind_subject = cv_group_sort(drug, ind_train, drug_labels)
% 
% drug          - group identifier: 1 = Biperiden, 2 = Amisulpride, 
%                 3 = Placebo
% ind_train     - logical array of the subjects in the training set
% drug_labels   - array coupling subject index, identifier and drug
%                 group
%__________________________________________________________________________
% 
% cv_group_sort returns the indices of subjects in the training set that
% belong to the group identified by 'drug'. This information is stored in
% the array 'drug_labels'.
%__________________________________________________________________________

ind_subject = [];
% Convert logical array to array of indices
training_set = 1:71;
training_set = training_set(ind_train);
% Iterate over the training set
for i = training_set
    % Get group from array containing the data
    drug_i = drug_labels(i, 2);
    % Check if in group 'drug', if yes: add to list to be returned
    if drug_i == drug
        ind_subject = [ind_subject i];
    end
end
end 