function F = cv_inversion_test(subject, D, res_DCM)
% Invert the DCM structures representing each drug group on a test subject.
% FORMAT F = cv_inversion_test(subject, D, res_DCM)
%
% subject        - Index identifying subject
% D              - MEEG object corresponding to subject
% res_DCM        - Cell array of DCM structures representing drug groups
%
% F              - Array of free energy for each DCM
%__________________________________________________________________________
% 
% cv_inversion_test inverts the DCM structures representing each drug group
% on a test subject, and returns the free energy corresponding to the DCM
% structures.
%__________________________________________________________________________

F = zeros(3, 1);

for drug = 1:3
    % Get DCM
    DCM = res_DCM{drug};
    % Remove subject specific fields
    DCM = rmfield(DCM, {'xY', 'name', 'Ep', 'Cp', 'Eg', 'Cg', 'Ce', 'Pp', 'H', 'K', 'x', 'R', 'F', 'L', 'ID'});
    % Set data from subject
    DCM.xY.Dfile = D.fullfile;
    % Invert
    DCM.name = ['Data' filesep 'Test_DCM_Analysis' filesep strcat('DCM_res0', int2str(subject), '_drug_', int2str(drug))];
    DCM = spm_dcm_erp(DCM);
    % Get free energy
    F(drug) = DCM.F;
end

end