function cv_main(full_DCM, no_lat_DCM, no_mod_DCM, D)
% Perform the cross validation.
% FORMAT cv_main(full_DCM, no_lat_DCM, no_mod_DCM, D)
%
% full_DCM   - Cell array of full DCM structures for each subject
% no_lat_DCM - Cell array of DCM structures without lateral 
%                     connections for each subject
% no_mod_DCM - Cell array of DCM structures without modulation on 
%                     the lateral connections for each subject
% D          - Cell array of M/EEG objects for each subject
%__________________________________________________________________________
% 
% cv_main performs the cross validation using the k-fold method for k = 7.
% For each training set, per drug group, PEB analysis is performed on each 
% model class to define empirical priors on the drug roup level. From these
% model classes with empirical priors, the DCM best representing each drug 
% group is selected by RFX BMS. For the test set, the classification is 
% performed based on the DCMs representing the drug groups: each 
% representative DCM is inverted on the test subject, and the subject is 
% classified according to the DCM that explained the test subject's data 
% best.
%
% Each subject is identified by the index corresponding to the subject in 
% "Data\pharma\dprst_anta_druglabels.csv", rather than by the primary
% identifying code as in the name of the MEEG objects.
%__________________________________________________________________________

% Reading csv file with drug label corresponding to subjects: 
% Biperiden = 1, Amisulpride = 2, Placebo = 3
drug_labels = readmatrix(['Data' filesep 'pharma' filesep 'dprst_anta_druglabels.csv']);

% Initiation of data to save
predictions = zeros(71, 1); % Array of predictions for all test subjects
F_test = zeros(71, 3); % Array of F for all test subjects for all drugs
ind_res_DCM = zeros(7, 3); % Array of index of best model for all folds and all drugs
                           % 1 : full model
                           % 2 : model without lateral connections
                           % 3 : model without modulation on lateral connections

% Split data
rng(100); % Seed
cv_dcm = cvpartition(71,"KFold", 7);

% For each partition
for i = 1:7
    ind_train = training(cv_dcm, i); % Logical array
    ind_test = test(cv_dcm, i); % Logical array

    % TRAINING
    res_DCM = cell(3, 1); % Initialize
    % For each group of drug
    for drug = 1:3
        % Define groups within training set
        ind_subject = cv_group_sort(drug, ind_train, drug_labels);

        % Get DCMs of subjects in training set
        P_full = cv_construction(full_DCM, ind_subject);
        P_no_lat = cv_construction(no_lat_DCM, ind_subject);
        P_no_mod = cv_construction(no_mod_DCM, ind_subject);
    
        % Parametric Empirical Bayes, without group effects
        [~, P_PEB_full] = spm_dcm_peb(P_full); 
        [~, P_PEB_no_lat] = spm_dcm_peb(P_no_lat); 
        [~, P_PEB_no_mod] = spm_dcm_peb(P_no_mod);
        % Make one cell array
        P_PEB = cv_concatenate(P_PEB_full, P_PEB_no_lat, P_PEB_no_mod);

        % Bayesian Model Selection 
        [~,exp_r,~,pxp,~,~] = spm_dcm_bmc(P_PEB);
        [best_DCM, ind, max_exp_r, max_pxp] = cv_bms(exp_r, pxp, P_PEB);
        % Store whether full, no_lat or no_mod is the best model
        ind_res_DCM(i, drug) = ind;
        % Store the best model
        res_DCM{drug} = best_DCM;
        
        % Save PEB results per fold and per drug for all models 
        save(['Data' filesep 'P_PEB' filesep strcat('P_PEB_fold', int2str(i), '_drug', int2str(drug))], "P_PEB")
        % Save RFX expectation of the posterior  p(m|y) per fold and per drug for all models
        save(['Data' filesep 'exp_r' filesep strcat('exp_r_fold', int2str(i), '_drug', int2str(drug))], "exp_r")
        % Save RFX protected exceedance probabilities per fold and per drug for all models
        save(['Data' filesep 'pxp' filesep strcat('pxp_fold', int2str(i), '_drug', int2str(drug))], "pxp")
    end

    % TEST    
    % Convert logical array to array of indices
    test_set = 1:71;
    test_set = test_set(ind_test);
    for subject = test_set
        % Invert DCMs representing drug groups on test subject
        F = cv_inversion_test(subject, D{subject}, res_DCM);
        F_test(subject, :) = F;
        % Select best fitting DCM
        [max_F, argmax_F] = max(F);
        % Classification based on DCM representing drug groups
        predictions(subject) = argmax_F;
    end

end

save(['Data' filesep 'predictions'], "predictions")
save(['Data' filesep 'F_test'], "F_test")
save(['Data' filesep 'ind_res_DCM'], "ind_res_DCM")

end