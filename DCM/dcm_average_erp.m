function D_average = dcm_average_erp(D)
% Average ERPs over trials per condition.
% FORMAT D_average = dcm_average_erp(D)
% 
% D             - M/EEG object
%
% D_average     - M/EEG object with data averaged per condition
%__________________________________________________________________________
% 
% dcm_average_erp averages ERPs in all channels over the trials per
% condition. The averaged M/EEG object is automatically saved in the same
% folder as the original M.EEG object, with the prefix 'm'.
%__________________________________________________________________________

% Define conditions
[condlist, ntrials] = dcm_define_conditions(D);
% Set conditions
D = conditions(D, 1: ntrials, condlist);
% Make structure
S.D = D;
% Set prefix to determine output folder
S.prefix = ['Data' filesep 'Averaged_D' filesep 'm'];
% Apply averaging
D_average = spm_eeg_average(S);
end