function [condlist, ntrials] = dcm_define_conditions(D)
% Define conditions for trials.
% FORMAT [condlist, ntrials] = dcm_define_conditions(D)
% 
% D             - M/EEG object
%
% condlist      - List of conditions for trials
% ntrials       - Number of trials
%__________________________________________________________________________
% 
% dcm_define_conditions defines the conditions for trials, in terms of
% 'standard', 'deviant' or 'other'. 'Standard' is defined as a trial, that
% is the same as the previous five trials. 'Deviant' is defined as a trial,
% that is not equal to the previous five trials. A trial is 'other' if it
% is neither 'standard' nor 'deviant'.
%__________________________________________________________________________

% Load stimulus sequence
stim_file = ['.' filesep 'Data' filesep 'paradigm' filesep 'stimulus_sequence'];
stim_seq = load(stim_file);
tones = stim_seq.tones;

% Get number of trials
ntrials = numel(tones);

% Get current list of conditions
condlist = D.conditions;

% Overwrite list of conditions
for i = 1:ntrials
    % The first 5 trials cannot be identified as either standard or
    % deviant.
    if i < 6
        condlist{i} = 'other';
    % Check if trial is 'standard'
    elseif (tones(i-5) == tones(i-4))&&(tones(i-4) == tones(i-3))&&(tones(i-3) == tones(i-2))&&(tones(i-2) == tones(i-1))&&(tones(i-1) == tones(i))
        condlist{i} = 'standard';
    % Check if trial is 'deviant'
    elseif (tones(i-5) == tones(i-4))&&(tones(i-4) == tones(i-3))&&(tones(i-3) == tones(i-2))&&(tones(i-2) == tones(i-1))&&(tones(i-1) ~= tones(i))
        condlist{i} = 'deviant';
    % If neither 'standard' nor 'deviant', trial is 'other'
    else
        condlist{i} = 'other';
    end
end
end