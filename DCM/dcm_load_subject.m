function D = dcm_load_subject(subject)
% Load an M/EEG file in SPM format for a given subject.
% FORMAT D = dcm_load_subject(subject)
% 
% subject       - Subject identifier, last 3 numbers of subject code
%
% D             - MEEG object
%__________________________________________________________________________
% 
% dcm_load_subject loads an M/EEG file using the SPM MEEG format when given
% the subject identifier as the last 3 numbers of the subject code.
%__________________________________________________________________________

% Convert subject identifier to string
subject = string(subject);
% Define filepath
anta_folder = ['.' filesep 'Data' filesep 'anta' filesep];
folder = char("DPRST_0" + string(subject));
file = char("DPRST_0" + string(subject) + "_MMN_preproc.mat");
subject_path = [folder filesep file];
eeg_file = fullfile(anta_folder, subject_path);
% Load MEEG file
D = spm_eeg_load(eeg_file);
end