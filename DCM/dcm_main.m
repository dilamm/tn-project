function dcm_main()
% Pre-process M/EEG object and invert all DCM classes for all subjects.
% FORMAT dcm_main()
%__________________________________________________________________________
% 
% dcm_main pre-processes the M/EEG object for each subject by averaging.
% Then, the DCM classes are constructed and inverted for each subject.
%__________________________________________________________________________

% Apply to all subjects
% 106 and 160 excluded because of failure to correct for eyeblinks
% 111, 125, 130, 131, 150, 155, 162 and 171 excluded because of missing data
for subject = [101:105 107:110 112:124 126:129 132:149 151:154 156:159 161 163:170 172:181]
    % Load M/EEG object for the subject
    D = dcm_load_subject(subject);
    % Average over trials
    D_average = dcm_average_erp(D);
    % Construct and invert full DCM
    DCM_full = dcm_full_inversion(D_average, subject);
    DCM_no_lat = dcm_no_lat_inversion(D_average, subject);
    DCM_no_mod = dcm_no_mod_inversion(D_average, subject);
end
end