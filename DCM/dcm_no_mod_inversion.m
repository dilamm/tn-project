function DCM = dcm_no_mod_inversion(D, subject)
% Construct and invert the DCM class without modulation on the lateral
% connections.
% FORMAT DCM = dcm_no_mod_inversion(D, subject)
%
% D             - Averaged M/EEG object
% subject       - Subject identifier, last 3 numbers of subject code
%
% DCM           - DCM without modulation on the lateral connections
%__________________________________________________________________________
% 
% dcm_no_mod_inversion constructs and inverts the DCM without modulation on
% the lateral connectionsfor the subject. The DCM is also saved in 
% 'No_Mod_DCM_Analysis/'.
%
% Based on spm12/man/example_scripts/DCM_ERP_subject1.m, distributed with
% the SPM12 software.
%__________________________________________________________________________

% Data filename
DCM.xY.Dfile = D.fullfile;

% Parameters and options used for setting up model
DCM.options.analysis = 'ERP'; % analyze evoked responses
DCM.options.model    = 'NMDA'; % ERP model
DCM.options.spatial  = 'ECD'; % spatial model
DCM.options.trials   = [2 3]; % index of ERPs within ERP/ERF file
DCM.options.Tdcm(1)  = 0;     % start of peri-stimulus time to be modelled
DCM.options.Tdcm(2)  = 300;   % end of peri-stimulus time to be modelled
DCM.options.Nmodes   = 8;     % nr of modes for data selection
DCM.options.h        = 1;     % nr of DCT components
DCM.options.onset    = 60;    % selection of onset (prior mean)
DCM.options.D        = 1;     % downsampling

% Data and spatial model
DCM = spm_dcm_erp_data(DCM);

% Location priors for dipoles
DCM.Lpos  = [[-42; -22; 7] [46; -14; 8] [-61; -32; 8] [59; -25; 8] [46; 20; 8]];
DCM.Sname = {'left A1', 'right A1', 'left STG', 'right STG', 'right IFG'};
Nareas    = size(DCM.Lpos,2);

% Spatial model
DCM = spm_dcm_erp_dipfit(DCM);

DCM.M.dipfit.Lpos

% Specify connectivity model
DCM.A{1} = zeros(Nareas,Nareas);
DCM.A{1}(3,1) = 1;
DCM.A{1}(4,2) = 1;
DCM.A{1}(5,4) = 1;

DCM.A{2} = zeros(Nareas,Nareas);
DCM.A{2}(1,3) = 1;
DCM.A{2}(2,4) = 1;
DCM.A{2}(4,5) = 1;

DCM.A{3} = zeros(Nareas,Nareas);
DCM.A{3}(4,3) = 1;
DCM.A{3}(3,4) = 1;

DCM.B{1} = DCM.A{1} + DCM.A{2};
DCM.B{1}(1,1) = 1;
DCM.B{1}(2,2) = 1;

DCM.C = [1; 1; 0; 0; 0];

% Between trial effects
DCM.xU.X = [0; 1];
DCM.xU.name = {'deviant'};

% Invert
DCM.name = ['Data' filesep 'No_Mod_DCM_Analysis' filesep strcat('DCM_no_mod0', int2str(subject))];
DCM = spm_dcm_erp(DCM);

end