This folder contains all sensor level EEG data for TN project work. 

EEG data for the antagonist branch is stored in the folder anta, for the agonist branch in the folder agon.

The folder pharma contains all information regarding drug administration for the two study branches separately. These are the drugs that should be classified by the model. 

The folder paradigm contains an image illustrating the probability strcuture and a mat file with the tone sequence. 

Details about data acquisition, preprocessing and a classical sensor level analysis
 are provided in the original paper (see reference below).

LA Weber, S Tomiello, D Schöbi, KV Wellstein, D Mueller, S Iglesias, KE Stephan (2022) 
Auditory mismatch responses are differentially sensitive to changes in muscarinic acetylcholine versus dopamine receptor function
eLife 11:e74835 (https://doi.org/10.7554/eLife.74835) 