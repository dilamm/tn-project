function accuracy = metrics_accuracy(drug_labels, predictions) 
% Compute the accuracy of the predictions.
% FORMAT accuracy = metrics_accuracy(drug_labels, predictions)  
% 
% drug_labels     - Target drug labels per subject 
% predictions     - Predicted drug labels 
%
% accuracy        - Accuracy of the predictions
% _________________________________________________________________________
% 
% metrics_accuracy calculates the accuracy of
% the predicted drug label compared to the target drug label.  
%__________________________________________________________________________

n = numel(predictions);

total_accuracies = zeros(n,1); 

target_labels = drug_labels(:,2); % Target drug labels for each subject

% Calculate if predictions correspond to drug taken by subject
for subject = 1:n
    subject_target = target_labels(subject); % Target drug label
    subject_prediction = predictions(subject); % Prediction drug label
    
    % Change values to 1 if the target and predictions are the same models
    if (subject_target == subject_prediction)
        total_accuracies(subject) = 1;  
    end

end 

% Calculate total accuracy
accuracy = mean(total_accuracies);


end
