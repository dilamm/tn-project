function dist = metrics_distance()
% Compute the distance between empirical priors of representative models.
% FORMAT dist = metrics_distance()
%
% dist      - Array containing the distances
%             Row 1: difference between 1 and 2
%             Row 2: difference between 1 and 3
%             Row 3: difference between 2 and 3
%__________________________________________________________________________
% 
% metrics_distance computes the distance between empirical priors of the
% representative models for the drug groups calculated on the full data.
% The distance was computed as '1 - the shared area under the curve 
% of the probability distributions'. This measure was computed for all 
% priors representing an extrinsic connection or modulation in one of the 
% model classes.
%__________________________________________________________________________

% Load PEB results for each drug
P_PEB1 = load(['Data' filesep 'FullData' filesep 'P_PEB' filesep 'P_PEB_drug1'], "P_PEB");
P_PEB2 = load(['Data' filesep 'FullData' filesep 'P_PEB' filesep 'P_PEB_drug2'], "P_PEB");
P_PEB3 = load(['Data' filesep 'FullData' filesep 'P_PEB' filesep 'P_PEB_drug3'], "P_PEB");
P_PEB1 = P_PEB1.P_PEB;
P_PEB2 = P_PEB2.P_PEB;
P_PEB3 = P_PEB3.P_PEB;

ind = load(['Data' filesep 'FullData' filesep 'ind_res_DCM'], "ind_res_DCM");
ind = ind.ind_res_DCM;

DCM1 = P_PEB1{1, ind(1)};
DCM2 = P_PEB2{1, ind(2)};
DCM3 = P_PEB3{1, ind(3)};

mu1 = full(spm_vec(DCM1.M.pE));
S1 = full(DCM1.M.pC);
mu2 = full(spm_vec(DCM2.M.pE));
S2 = full(DCM2.M.pC);
mu3 = full(spm_vec(DCM3.M.pE));
S3 = full(DCM3.M.pC);

dist = zeros(3, 18); % Row 1: difference between 1 and 2
                     % Row 2: difference between 1 and 3
                     % Row 3: difference between 2 and 3
j = 1;

for i = [14 20 31 47 53 60 75 79 87 89 93 95 97 100 103 104 106 110] % Get relevant parameters in fields A and B
    m1 = mu1(i);
    m2 = mu2(i);
    m3 = mu3(i);
    var1 = S1(i, i);
    var2 = S2(i, i);
    var3 = S3(i, i);
    

    % Difference between 1 and 2
    if m1 > m2
	    m_tmp = m1;
            m1 = m2;
            m2 = m_tmp;
               
            var_tmp = var1;
            var1 = var2;
            var2 = var_tmp;
    end
    
    % In case of point prior
    if (var1 == 0) & (var2 == 0) 
        if m1 == m2
            dist(1, j) = 1;
        end
    elseif var1 == 0
        dist(1, j) = normpdf(m1, m2, sqrt(var2));
    elseif var2 == 0
        dist(1, j) = normpdf(m2, m1, sqrt(var1));
    else
        % Compute the intersection point of the distributions
        if var1 == var2
            c = (m1+m2)/2;
        else
            c = (m2*var1 - sqrt(var2)*(m1*sqrt(var2)+sqrt(var1)*sqrt((m1-m2).^2 + 2*(var1-var2)*log(sqrt(var1)/sqrt(var2)))))/(var1-var2);
        end
        % Compute the area (probability of being equal)
        area = 1 - 0.5*erf((c-m1)/(sqrt(2)*sqrt(var1))) + 0.5*erf((c-m2)/(sqrt(2)*sqrt(var2)));
        
        % 0 if equal, 1 if no overlap
        dist(1, j) = 1 - area;
    end


    % Difference between 1 and 3
    if m1 > m3
	    m_tmp = m1;
            m1 = m3;
            m3 = m_tmp;
               
            var_tmp = var1;
            var1 = var3;
            var3 = var_tmp;
    end

    % In case of point prior
    if (var1 == 0) & (var3 == 0) 
        if m1 == m3
            dist(2, j) = 1;
        end
    elseif var1 == 0
        dist(2, j) = normpdf(m1, m3, sqrt(var3));
    elseif var3 == 0
        dist(2, j) = normpdf(m3, m1, sqrt(var1));
    else
        % Compute the intersection point of the distributions
        if var1 == var3
            c = (m1+m3)/2;
        else
            c = (m3*var1 - sqrt(var3)*(m1*sqrt(var3)+sqrt(var1)*sqrt((m1-m3).^2 + 2*(var1-var3)*log(sqrt(var1)/sqrt(var3)))))/(var1-var3);
        end
        % Compute the area (probability of being equal)
        area = 1 - 0.5*erf((c-m1)/(sqrt(2)*sqrt(var1))) + 0.5*erf((c-m3)/(sqrt(2)*sqrt(var3)));
        
        % 0 if equal, 1 if no overlap
        dist(2, j) = 1 - area;
    end


    % Difference between 2 and 3
    if m2 > m3
	    m_tmp = m2;
            m2 = m3;
            m3 = m_tmp;
               
            var_tmp = var2;
            var2 = var3;
            var3 = var_tmp;
    end

    % In case of point prior
    if (var2 == 0) & (var3 == 0) 
        if m2 == m3
            dist(3, j) = 1;
        end
    elseif var2 == 0
        dist(3, j) = normpdf(m2, m3, sqrt(var3));
    elseif var3 == 0
        dist(3, j) = normpdf(m3, m2, sqrt(var2));
    else
        % Compute the intersection point of the distributions
        if var2 == var3
            c = (m2+m3)/2;
        else
            c = (m3*var2 - sqrt(var3)*(m2*sqrt(var3)+sqrt(var2)*sqrt((m2-m3).^2 + 2*(var2-var3)*log(sqrt(var2)/sqrt(var3)))))/(var2-var3);
        end
        % Compute the area (probability of being equal)
        area = 1 - 0.5*erf((c-m2)/(sqrt(2)*sqrt(var2))) + 0.5*erf((c-m3)/(sqrt(2)*sqrt(var3)));
        
        % 0 if equal, 1 if no overlap
        dist(3, j) = 1 - area;
    end

    j = j+1;
end

end