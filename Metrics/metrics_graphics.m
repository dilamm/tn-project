function metrics_graphics(predictions, drug_labels, dist, sim)
% Create graphical representations of the data.
% FORMAT metrics_graphics(predictions, drug_labels, dist, sim)
%
% target_labels   - Target drug labels per subject 
% predictions     - Predicted drug labels per subject
% dist            - Array containing the distances
%                   Row 1: difference between 1 and 2
%                   Row 2: difference between 1 and 3
%                   Row 3: difference between 2 and 3
% sim             - Struct with fields 'standard', 'deviant' and 'MMN'
%                   containing the measured and simulated average responses 
%                   and SEM for all drug groups and all model classes
%__________________________________________________________________________
% 
% metrics_graphics creates graphical representations of the results.
%__________________________________________________________________________

% Simulations
t = 0:4:300;

% Figure for standard condition
figure
fig_sim_std = tiledlayout(3, 3, 'TileSpacing', 'loose', 'Padding','loose');
sgtitle('Simulations for the standard condition');

nexttile
title('Biperiden, Model Class 1');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.standard{1, 1}.m_meas - 2*sim.standard{1, 1}.SEM_meas, t, sim.standard{1, 1}.m_meas + 2*sim.standard{1, 1}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.standard{1, 1}.m_sim - 2*sim.standard{1, 1}.SEM_sim, t, sim.standard{1, 1}.m_sim + 2*sim.standard{1, 1}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.standard{1, 1}.m_meas, 'r');
plot(t, sim.standard{1, 1}.m_sim, 'b');
hold off

nexttile
title('Biperiden, Model Class 2');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.standard{1, 2}.m_meas - 2*sim.standard{1, 2}.SEM_meas, t, sim.standard{1, 2}.m_meas + 2*sim.standard{1, 2}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.standard{1, 2}.m_sim - 2*sim.standard{1, 2}.SEM_sim, t, sim.standard{1, 2}.m_sim + 2*sim.standard{1, 2}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.standard{1, 2}.m_meas, 'r');
plot(t, sim.standard{1, 2}.m_sim, 'b');
hold off

nexttile
title('Biperiden, Model Class 3');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.standard{1, 3}.m_meas - 2*sim.standard{1, 3}.SEM_meas, t, sim.standard{1, 3}.m_meas + 2*sim.standard{1, 3}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.standard{1, 3}.m_sim - 2*sim.standard{1, 3}.SEM_sim, t, sim.standard{1, 3}.m_sim + 2*sim.standard{1, 3}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.standard{1, 3}.m_meas, 'r');
plot(t, sim.standard{1, 3}.m_sim, 'b');
hold off

nexttile
title('Amisulpride, Model Class 1');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.standard{2, 1}.m_meas - 2*sim.standard{2, 1}.SEM_meas, t, sim.standard{2, 1}.m_meas + 2*sim.standard{2, 1}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.standard{2, 1}.m_sim - 2*sim.standard{2, 1}.SEM_sim, t, sim.standard{2, 1}.m_sim + 2*sim.standard{2, 1}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.standard{2, 1}.m_meas, 'r');
plot(t, sim.standard{2, 1}.m_sim, 'b');
hold off

nexttile
title('Amisulpride, Model Class 2');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.standard{2, 2}.m_meas - 2*sim.standard{2, 2}.SEM_meas, t, sim.standard{2, 2}.m_meas + 2*sim.standard{2, 2}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.standard{2, 2}.m_sim - 2*sim.standard{2, 2}.SEM_sim, t, sim.standard{2, 2}.m_sim + 2*sim.standard{2, 2}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.standard{2, 2}.m_meas, 'r');
plot(t, sim.standard{2, 2}.m_sim, 'b');
hold off

nexttile
title('Amisulpride, Model Class 3');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.standard{2, 3}.m_meas - 2*sim.standard{2, 3}.SEM_meas, t, sim.standard{2, 3}.m_meas + 2*sim.standard{2, 3}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.standard{2, 3}.m_sim - 2*sim.standard{2, 3}.SEM_sim, t, sim.standard{2, 3}.m_sim + 2*sim.standard{2, 3}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.standard{2, 3}.m_meas, 'r');
plot(t, sim.standard{2, 3}.m_sim, 'b');
hold off

nexttile
title('Placebo, Model Class 1');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.standard{3, 1}.m_meas - 2*sim.standard{3, 1}.SEM_meas, t, sim.standard{3, 1}.m_meas + 2*sim.standard{3, 1}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.standard{3, 1}.m_sim - 2*sim.standard{3, 1}.SEM_sim, t, sim.standard{3, 1}.m_sim + 2*sim.standard{3, 1}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.standard{3, 1}.m_meas, 'r');
plot(t, sim.standard{3, 1}.m_sim, 'b');
hold off

nexttile
title('Placebo, Model Class 2');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.standard{3, 2}.m_meas - 2*sim.standard{3, 2}.SEM_meas, t, sim.standard{3, 2}.m_meas + 2*sim.standard{3, 2}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.standard{3, 2}.m_sim - 2*sim.standard{3, 2}.SEM_sim, t, sim.standard{3, 2}.m_sim + 2*sim.standard{3, 2}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.standard{3, 2}.m_meas, 'r');
plot(t, sim.standard{3, 2}.m_sim, 'b');
hold off

nexttile
title('Placebo, Model Class 3');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.standard{3, 3}.m_meas - 2*sim.standard{3, 3}.SEM_meas, t, sim.standard{3, 3}.m_meas + 2*sim.standard{3, 3}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.standard{3, 3}.m_sim - 2*sim.standard{3, 3}.SEM_sim, t, sim.standard{3, 3}.m_sim + 2*sim.standard{3, 3}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.standard{3, 3}.m_meas, 'r');
plot(t, sim.standard{3, 3}.m_sim, 'b');
hold off

saveas(fig_sim_std, ['Metrics' filesep 'simulations_standard.png'])


% Figure for deviant condition
figure
fig_sim_dev = tiledlayout(3, 3, 'TileSpacing', 'loose', 'Padding','loose');
sgtitle('Simulations for the deviant condition');

nexttile
title('Biperiden, Model Class 1');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.deviant{1, 1}.m_meas - 2*sim.deviant{1, 1}.SEM_meas, t, sim.deviant{1, 1}.m_meas + 2*sim.deviant{1, 1}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.deviant{1, 1}.m_sim - 2*sim.deviant{1, 1}.SEM_sim, t, sim.deviant{1, 1}.m_sim + 2*sim.deviant{1, 1}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.deviant{1, 1}.m_meas, 'r');
plot(t, sim.deviant{1, 1}.m_sim, 'b');
hold off

nexttile
title('Biperiden, Model Class 2');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.deviant{1, 2}.m_meas - 2*sim.deviant{1, 2}.SEM_meas, t, sim.deviant{1, 2}.m_meas + 2*sim.deviant{1, 2}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.deviant{1, 2}.m_sim - 2*sim.deviant{1, 2}.SEM_sim, t, sim.deviant{1, 2}.m_sim + 2*sim.deviant{1, 2}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.deviant{1, 2}.m_meas, 'r');
plot(t, sim.deviant{1, 2}.m_sim, 'b');
hold off

nexttile
title('Biperiden, Model Class 3');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.deviant{1, 3}.m_meas - 2*sim.deviant{1, 3}.SEM_meas, t, sim.deviant{1, 3}.m_meas + 2*sim.deviant{1, 3}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.deviant{1, 3}.m_sim - 2*sim.deviant{1, 3}.SEM_sim, t, sim.deviant{1, 3}.m_sim + 2*sim.deviant{1, 3}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.deviant{1, 3}.m_meas, 'r');
plot(t, sim.deviant{1, 3}.m_sim, 'b');
hold off

nexttile
title('Amisulpride, Model Class 1');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.deviant{2, 1}.m_meas - 2*sim.deviant{2, 1}.SEM_meas, t, sim.deviant{2, 1}.m_meas + 2*sim.deviant{2, 1}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.deviant{2, 1}.m_sim - 2*sim.deviant{2, 1}.SEM_sim, t, sim.deviant{2, 1}.m_sim + 2*sim.deviant{2, 1}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.deviant{2, 1}.m_meas, 'r');
plot(t, sim.deviant{2, 1}.m_sim, 'b');
hold off

nexttile
title('Amisulpride, Model Class 2');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.deviant{2, 2}.m_meas - 2*sim.deviant{2, 2}.SEM_meas, t, sim.deviant{2, 2}.m_meas + 2*sim.deviant{2, 2}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.deviant{2, 2}.m_sim - 2*sim.deviant{2, 2}.SEM_sim, t, sim.deviant{2, 2}.m_sim + 2*sim.deviant{2, 2}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.deviant{2, 2}.m_meas, 'r');
plot(t, sim.deviant{2, 2}.m_sim, 'b');
hold off

nexttile
title('Amisulpride, Model Class 3');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.deviant{2, 3}.m_meas - 2*sim.deviant{2, 3}.SEM_meas, t, sim.deviant{2, 3}.m_meas + 2*sim.deviant{2, 3}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.deviant{2, 3}.m_sim - 2*sim.deviant{2, 3}.SEM_sim, t, sim.deviant{2, 3}.m_sim + 2*sim.deviant{2, 3}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.deviant{2, 3}.m_meas, 'r');
plot(t, sim.deviant{2, 3}.m_sim, 'b');
hold off

nexttile
title('Placebo, Model Class 1');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.deviant{3, 1}.m_meas - 2*sim.deviant{3, 1}.SEM_meas, t, sim.deviant{3, 1}.m_meas + 2*sim.deviant{3, 1}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.deviant{3, 1}.m_sim - 2*sim.deviant{3, 1}.SEM_sim, t, sim.deviant{3, 1}.m_sim + 2*sim.deviant{3, 1}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.deviant{3, 1}.m_meas, 'r');
plot(t, sim.deviant{3, 1}.m_sim, 'b');
hold off

nexttile
title('Placebo, Model Class 2');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.deviant{3, 2}.m_meas - 2*sim.deviant{3, 2}.SEM_meas, t, sim.deviant{3, 2}.m_meas + 2*sim.deviant{3, 2}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.deviant{3, 2}.m_sim - 2*sim.deviant{3, 2}.SEM_sim, t, sim.deviant{3, 2}.m_sim + 2*sim.deviant{3, 2}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.deviant{3, 2}.m_meas, 'r');
plot(t, sim.deviant{3, 2}.m_sim, 'b');
hold off

nexttile
title('Placebo, Model Class 3');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.deviant{3, 3}.m_meas - 2*sim.deviant{3, 3}.SEM_meas, t, sim.deviant{3, 3}.m_meas + 2*sim.deviant{3, 3}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.deviant{3, 3}.m_sim - 2*sim.deviant{3, 3}.SEM_sim, t, sim.deviant{3, 3}.m_sim + 2*sim.deviant{3, 3}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.deviant{3, 3}.m_meas, 'r');
plot(t, sim.deviant{3, 3}.m_sim, 'b');
hold off

saveas(fig_sim_dev, ['Metrics' filesep 'simulations_deviant.png'])


% Figure for MMN
figure
fig_sim_mmn = tiledlayout(3, 3, 'TileSpacing', 'loose', 'Padding','loose');
sgtitle('Comparison of MMN predictions and measurements');

nexttile
title('Biperiden, Model Class 1');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.MMN{1, 1}.m_meas - 2*sim.MMN{1, 1}.SEM_meas, t, sim.MMN{1, 1}.m_meas + 2*sim.MMN{1, 1}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.MMN{1, 1}.m_sim - 2*sim.MMN{1, 1}.SEM_sim, t, sim.MMN{1, 1}.m_sim + 2*sim.MMN{1, 1}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.MMN{1, 1}.m_meas, 'r');
plot(t, sim.MMN{1, 1}.m_sim, 'b');
hold off

nexttile
title('Biperiden, Model Class 2');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.MMN{1, 2}.m_meas - 2*sim.MMN{1, 2}.SEM_meas, t, sim.MMN{1, 2}.m_meas + 2*sim.MMN{1, 2}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.MMN{1, 2}.m_sim - 2*sim.MMN{1, 2}.SEM_sim, t, sim.MMN{1, 2}.m_sim + 2*sim.MMN{1, 2}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.MMN{1, 2}.m_meas, 'r');
plot(t, sim.MMN{1, 2}.m_sim, 'b');
hold off

nexttile
title('Biperiden, Model Class 3');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.MMN{1, 3}.m_meas - 2*sim.MMN{1, 3}.SEM_meas, t, sim.MMN{1, 3}.m_meas + 2*sim.MMN{1, 3}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.MMN{1, 3}.m_sim - 2*sim.MMN{1, 3}.SEM_sim, t, sim.MMN{1, 3}.m_sim + 2*sim.MMN{1, 3}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.MMN{1, 3}.m_meas, 'r');
plot(t, sim.MMN{1, 3}.m_sim, 'b');
hold off

nexttile
title('Amisulpride, Model Class 1');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.MMN{2, 1}.m_meas - 2*sim.MMN{2, 1}.SEM_meas, t, sim.MMN{2, 1}.m_meas + 2*sim.MMN{2, 1}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.MMN{2, 1}.m_sim - 2*sim.MMN{2, 1}.SEM_sim, t, sim.MMN{2, 1}.m_sim + 2*sim.MMN{2, 1}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.MMN{2, 1}.m_meas, 'r');
plot(t, sim.MMN{2, 1}.m_sim, 'b');
hold off

nexttile
title('Amisulpride, Model Class 2');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.MMN{2, 2}.m_meas - 2*sim.MMN{2, 2}.SEM_meas, t, sim.MMN{2, 2}.m_meas + 2*sim.MMN{2, 2}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.MMN{2, 2}.m_sim - 2*sim.MMN{2, 2}.SEM_sim, t, sim.MMN{2, 2}.m_sim + 2*sim.MMN{2, 2}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.MMN{2, 2}.m_meas, 'r');
plot(t, sim.MMN{2, 2}.m_sim, 'b');
hold off

nexttile
title('Amisulpride, Model Class 3');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.MMN{2, 3}.m_meas - 2*sim.MMN{2, 3}.SEM_meas, t, sim.MMN{2, 3}.m_meas + 2*sim.MMN{2, 3}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.MMN{2, 3}.m_sim - 2*sim.MMN{2, 3}.SEM_sim, t, sim.MMN{2, 3}.m_sim + 2*sim.MMN{2, 3}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.MMN{2, 3}.m_meas, 'r');
plot(t, sim.MMN{2, 3}.m_sim, 'b');
hold off

nexttile
title('Placebo, Model Class 1');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.MMN{3, 1}.m_meas - 2*sim.MMN{3, 1}.SEM_meas, t, sim.MMN{3, 1}.m_meas + 2*sim.MMN{3, 1}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.MMN{3, 1}.m_sim - 2*sim.MMN{3, 1}.SEM_sim, t, sim.MMN{3, 1}.m_sim + 2*sim.MMN{3, 1}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.MMN{3, 1}.m_meas, 'r');
plot(t, sim.MMN{3, 1}.m_sim, 'b');
hold off

nexttile
title('Placebo, Model Class 2');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.MMN{3, 2}.m_meas - 2*sim.MMN{3, 2}.SEM_meas, t, sim.MMN{3, 2}.m_meas + 2*sim.MMN{3, 2}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.MMN{3, 2}.m_sim - 2*sim.MMN{3, 2}.SEM_sim, t, sim.MMN{3, 2}.m_sim + 2*sim.MMN{3, 2}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.MMN{3, 2}.m_meas, 'r');
plot(t, sim.MMN{3, 2}.m_sim, 'b');
hold off

nexttile
title('Placebo, Model Class 3');
xlabel('Time [ms]')
ylabel('EEG [a.u.]')
hold on
shade(t, sim.MMN{3, 3}.m_meas - 2*sim.MMN{3, 3}.SEM_meas, t, sim.MMN{3, 3}.m_meas + 2*sim.MMN{3, 3}.SEM_meas, 'FillColor', 'r', 'FillType', [2, 1]);
shade(t, sim.MMN{3, 3}.m_sim - 2*sim.MMN{3, 3}.SEM_sim, t, sim.MMN{3, 3}.m_sim + 2*sim.MMN{3, 3}.SEM_sim, 'FillColor', 'b', 'FillType', [2, 1]);
plot(t, sim.MMN{3, 3}.m_meas, 'r');
plot(t, sim.MMN{3, 3}.m_sim, 'b');
hold off

saveas(fig_sim_mmn, ['Metrics' filesep 'simulations_MMN.png'])



% Distance between representative models
fig_dist = figure;

subplot(3,1,1); 
histogram(dist(1, :), 10); 
title('Distance between representative models for biperiden and amisulpride');
xlabel('1 - shared AUC');
ylabel('# Parameters')

subplot(3,1,2); 
histogram(dist(2, :), 10); 
title('Distance between representative models for biperiden and placebo');
xlabel('1 - shared AUC');
ylabel('# Parameters')

subplot(3,1,3); 
histogram(dist(3, :), 10);
title('Distance between representative models for amisulpride and placebo');
xlabel('1 - shared AUC');
ylabel('# Parameters')

saveas(fig_dist, ['Metrics' filesep 'distance_histograms.png'])

% Confusion matrix : Accuracy Test Data
% Shows visually the 3 drug groups and how good the models were predicted
fig_C = figure;
labels = {'Biperiden', 'Amisulpride', 'Placebo'}; 
m = confusionmat(drug_labels(:,2), predictions); 
C = confusionchart(m, labels);
C.RowSummary = 'row-normalized'; 
C.ColumnSummary = 'column-normalized'; 
C.Title = 'Confusion Matrix Test Data';

saveas(fig_C, ['Metrics' filesep 'confusion_matrix.png']) 


% Free energy per test subject per model
F_test = load(['Data' filesep 'F_test.mat'], 'F_test'); 
F_test = F_test.F_test; 

fig_F = figure;
subplot(3,1,1); 
bar(1:25, F_test(1:25,:));
xticks(1:25);
title('Free Energy per DCM Model'); 

subplot(3,1,2); 
bar(26:50, F_test(26:50,:)); 
xticks(26:50);
ylabel('Free Energy');

subplot(3,1,3); 
bar(50:71, F_test(50:71,:));
xticks(50:71);
xlabel('Subjects');


saveas(fig_F, ['Metrics' filesep 'free_energy.png'])

% Figures for PXP data
drug1_data = load(['Data' filesep 'FullData' filesep 'pxp' filesep 'pxp_drug1.mat'], 'pxp');
drug2_data = load(['Data' filesep 'FullData' filesep 'pxp' filesep 'pxp_drug2.mat'], 'pxp');
drug3_data = load(['Data' filesep 'FullData' filesep 'pxp' filesep 'pxp_drug3.mat'], 'pxp');

fig_pxp = figure;
sgtitle("Protected exceedance probability (PXP) values for all drugs");

subplot(1,3,1)
bar(drug1_data.pxp);
xticks(1:3)
ylim([0,0.5])
subtitle("Biperiden")
ylabel("PXP")

subplot(1,3,2)
bar(drug2_data.pxp);
xticks(1:3)
ylim([0,0.5])
subtitle("Amisulpride")
xlabel("Model Class")

subplot(1,3,3)
bar(drug3_data.pxp);
xticks(1:3)
ylim([0,0.5])
subtitle("Placebo")

saveas(fig_pxp, ['Metrics' filesep 'PxP.png']) 

end