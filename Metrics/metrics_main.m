function metrics_main()
% Create the informative and graphical representations of the data.
% FORMAT metrics_main()
%__________________________________________________________________________
% 
% metrics_main analyses the data acquired by the DCM analysis, PEB analysis
% and classification and creates informative and graphical representations
% of our results. 
%__________________________________________________________________________

% Reading csv file with drug label corresponding to subjects: 
% Biperiden = 1, Amisulpride = 2, Placebo = 3
drug_labels = readmatrix(['Data' filesep 'pharma' filesep 'dprst_anta_druglabels.csv']);

% DCM results
[sim, explained_var] = metrics_simulations();
disp('Explained variance standard condition, drug groups x model classes: ')
disp(explained_var.standard)
save(['Metrics' filesep 'expl_var_std'], 'explained_var')
disp('Explained variance deviant condition, drug groups x model classes: ')
disp(explained_var.deviant)
save(['Metrics' filesep 'expl_var_dev'], 'explained_var')
disp('Explained variance MMN, drug groups x model classes: ')
disp(explained_var.MMN)
save(['Metrics' filesep 'expl_var_mmn'], 'explained_var')


% PEB results
metrics_peb_analysis() 
dist = metrics_distance();

% Classification results
predictions = load(['Data' filesep 'predictions.mat'], 'predictions');
predictions = predictions.predictions;
accuracy = metrics_accuracy(drug_labels, predictions); 
disp('Accuracy = ')
disp(accuracy)
save(['Metrics' filesep 'accuracy'], 'accuracy')

% Create graphical representations
metrics_graphics(predictions, drug_labels, dist, sim); 

end