function metrics_peb_analysis()
% Perform the PEB analysis on the full dataset.
% FORMAT metrics_peb_analysis()
%__________________________________________________________________________
%
% metrics_peb_analysis performs the PEB analysis on the full dataset.
% Empirical priors are computed per drug group for all model classes, and
% BMS is used to select the representative model.
%__________________________________________________________________________

% Reading csv file with drug label corresponding to subjects: 
% Biperiden = 1, Amisulpride = 2, Placebo = 3
drug_labels = readmatrix(['Data' filesep 'pharma' filesep 'dprst_anta_druglabels.csv']);

% Get necessary input
full_DCM = matfiles_to_array(['Data' filesep 'Full_DCM_Analysis'], "DCM"); % Full model
no_lat_DCM = matfiles_to_array(['Data' filesep 'No_Lat_DCM_Analysis'], "DCM"); % Model without lateral connections 
no_mod_DCM = matfiles_to_array(['Data' filesep 'No_Mod_DCM_Analysis'], "DCM"); % Model without lateral modulations

% Initialize
res_DCM = cell(1, 3);
ind_res_DCM = zeros(1, 3); % Array of index of best model for all drugs
                           % 1 : full model
                           % 2 : model without lateral connections
                           % 3 : model without modulation on lateral connections

% For each group of drug
for drug = 1:3
    % Define groups within training set
    ind_subject = cv_group_sort(drug, logical(ones(1, 71)), drug_labels);
    
    % Get DCMs of subjects in training set
    P_full = cv_construction(full_DCM, ind_subject);
    P_no_lat = cv_construction(no_lat_DCM, ind_subject);
    P_no_mod = cv_construction(no_mod_DCM, ind_subject);

    % Parametric Empirical Bayes, without group effects
    [~, P_PEB_full] = spm_dcm_peb(P_full);
    [~, P_PEB_no_lat] = spm_dcm_peb(P_no_lat);
    [~, P_PEB_no_mod] = spm_dcm_peb(P_no_mod);
    % Make one cell array
    P_PEB = cv_concatenate(P_PEB_full, P_PEB_no_lat, P_PEB_no_mod);

    % Bayesian Model Selection 
    [~,exp_r,~,pxp,~,~] = spm_dcm_bmc(P_PEB);
    [best_DCM, ind, max_exp_r, max_pxp] = cv_bms(exp_r, pxp, P_PEB);
    % Store the best model
    ind_res_DCM(drug) = ind;
    res_DCM{drug} = best_DCM;
    
    % Save PEB results per fold and per drug for all models 
    save(['Data' filesep 'FullData' filesep 'P_PEB' filesep strcat('P_PEB_drug', int2str(drug))], 'P_PEB')
    % Save RFX expectation of the posterior  p(m|y) and per drug for all models
    save(['Data' filesep 'FullData' filesep 'exp_r' filesep strcat('exp_r_drug', int2str(drug))], 'exp_r')
    % Save RFX protected exceedance probabilities and per drug for all models
    save(['Data' filesep 'FullData' filesep 'pxp' filesep strcat('pxp_drug', int2str(drug))], 'pxp')
end

save(['Data' filesep 'FullData' filesep 'ind_res_DCM'], 'ind_res_DCM')

end