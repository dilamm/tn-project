function [sim, explained_var] = metrics_simulations()
% Simulate the individual subject responses.
% FORMAT [sim, explained_var] = metrics_simulations()
%
% sim           - Struct with fields 'standard', 'deviant' and 'MMN'
%                 containing the measured and simulated average responses 
%                 and SEM for all drug groups and all model classes
% explained var - Struct with fields 'standard', 'deviant' and 'MMN'
%                 containing the explained variance for all drug
%                 groups and all model classes
%__________________________________________________________________________
%
% metrics_simulations simulates the average of the individual subject 
% responses for the three model classes, in both the standard and deviant
% conditions and for the MMN, and compares this to the average measured
% responses using explained variance.
%__________________________________________________________________________

% Reading csv file with drug label corresponding to subjects: 
% Biperiden = 1, Amisulpride = 2, Placebo = 3
drug_labels = readmatrix(['Data' filesep 'pharma' filesep 'dprst_anta_druglabels.csv']);

% Initialize
sim.standard = cell(3, 3);
sim.deviant = cell(3, 3);
sim.MMN = cell(3, 3);

% Choose channel
channel = 60; % FCz

% Load data
full_DCM = matfiles_to_array(['Data' filesep 'Full_DCM_Analysis'], "DCM"); % Full model
no_lat_DCM = matfiles_to_array(['Data' filesep 'No_Lat_DCM_Analysis'], "DCM"); % Model without lateral connections 
no_mod_DCM = matfiles_to_array(['Data' filesep 'No_Mod_DCM_Analysis'], "DCM"); % Model without lateral modulations

for drug = 1:3
    % Get subjects in drug group
    ind_subject = cv_group_sort(drug, logical(ones(1, 71)), drug_labels);
    n = numel(ind_subject);
    
    % Initialize
    measured_std_full = zeros(n, 76);
    measured_dev_full = zeros(n, 76);
    measured_std_no_lat = zeros(n, 76);
    measured_dev_no_lat = zeros(n, 76);
    measured_std_no_mod = zeros(n, 76);
    measured_dev_no_mod = zeros(n, 76);

    simulations_std_full = zeros(n, 76);
    simulations_dev_full = zeros(n, 76);
    simulations_std_no_lat = zeros(n, 76);
    simulations_dev_no_lat = zeros(n, 76);
    simulations_std_no_mod = zeros(n, 76);
    simulations_dev_no_mod = zeros(n, 76);

    j = 1;
    for i = ind_subject
        % Get normalized measured data
        measured_std_full(j, :) = zscore(full_DCM{i}.xY.y{1}(:, channel));
        measured_dev_full(j, :) = zscore(full_DCM{i}.xY.y{2}(:, channel));
        measured_std_no_lat(j, :) = zscore(no_lat_DCM{i}.xY.y{1}(:, channel));
        measured_dev_no_lat(j, :) = zscore(no_lat_DCM{i}.xY.y{2}(:, channel));
        measured_std_no_mod(j, :) = zscore(no_mod_DCM{i}.xY.y{1}(:, channel));
        measured_dev_no_mod(j, :) = zscore(no_mod_DCM{i}.xY.y{2}(:, channel));
        
        % Get normalized simulated data
        full_std = full_DCM{i}.H{1}*full(full_DCM{i}.M.U)';
        full_dev = full_DCM{i}.H{2}*full(full_DCM{i}.M.U)';
        no_lat_std = no_lat_DCM{i}.H{1}*full(no_lat_DCM{i}.M.U)';
        no_lat_dev = no_lat_DCM{i}.H{2}*full(no_lat_DCM{i}.M.U)';
        no_mod_std = no_mod_DCM{i}.H{1}*full(no_mod_DCM{i}.M.U)';
        no_mod_dev = no_mod_DCM{i}.H{2}*full(no_mod_DCM{i}.M.U)';
        
        simulations_std_full(j, :) = zscore(full_std(:, channel));
        simulations_dev_full(j, :) = zscore(full_dev(:, channel));
        simulations_std_no_lat(j, :) = zscore(no_lat_std(:, channel));
        simulations_dev_no_lat(j, :) = zscore(no_lat_dev(:, channel));
        simulations_std_no_mod(j, :) = zscore(no_mod_std(:, channel));
        simulations_dev_no_mod(j, :) = zscore(no_mod_dev(:, channel));

        j = j+1;
    end
    
    % Get MMN
    measured_mmn_full = measured_dev_full - measured_std_full;
    simulations_mmn_full = simulations_dev_full - simulations_std_full;
    measured_mmn_no_lat = measured_dev_no_lat - measured_std_no_lat;
    simulations_mmn_no_lat = simulations_dev_no_lat - simulations_std_no_lat;
    measured_mmn_no_mod = measured_dev_no_mod - measured_std_no_mod;
    simulations_mmn_no_mod = simulations_dev_no_mod - simulations_std_no_mod;

    % Get average and SEM for standard condition
    sim.standard{drug, 1}.m_meas = mean(measured_std_full);
    sim.standard{drug, 1}.SEM_meas = std(measured_std_full)/sqrt(n);
    sim.standard{drug, 2}.m_meas = mean(measured_std_no_lat);
    sim.standard{drug, 2}.SEM_meas = std(measured_std_no_lat)/sqrt(n);
    sim.standard{drug, 3}.m_meas = mean(measured_std_no_mod);
    sim.standard{drug, 3}.SEM_meas = std(measured_std_no_mod)/sqrt(n);

    sim.standard{drug, 1}.m_sim = mean(simulations_std_full);
    sim.standard{drug, 1}.SEM_sim = std(simulations_std_full)/sqrt(n);
    sim.standard{drug, 2}.m_sim = mean(simulations_std_no_lat);
    sim.standard{drug, 2}.SEM_sim = std(simulations_std_no_lat)/sqrt(n);
    sim.standard{drug, 3}.m_sim = mean(simulations_std_no_mod);
    sim.standard{drug, 3}.SEM_sim = std(simulations_std_no_mod)/sqrt(n);

    % Get average and SEM for deviant condition
    sim.deviant{drug, 1}.m_meas = mean(measured_dev_full);
    sim.deviant{drug, 1}.SEM_meas = std(measured_dev_full)/sqrt(n);
    sim.deviant{drug, 2}.m_meas = mean(measured_dev_no_lat);
    sim.deviant{drug, 2}.SEM_meas = std(measured_dev_no_lat)/sqrt(n);
    sim.deviant{drug, 3}.m_meas = mean(measured_dev_no_mod);
    sim.deviant{drug, 3}.SEM_meas = std(measured_dev_no_mod)/sqrt(n);

    sim.deviant{drug, 1}.m_sim = mean(simulations_dev_full);
    sim.deviant{drug, 1}.SEM_sim = std(simulations_dev_full)/sqrt(n);
    sim.deviant{drug, 2}.m_sim = mean(simulations_dev_no_lat);
    sim.deviant{drug, 2}.SEM_sim = std(simulations_dev_no_lat)/sqrt(n);
    sim.deviant{drug, 3}.m_sim = mean(simulations_dev_no_mod);
    sim.deviant{drug, 3}.SEM_sim = std(simulations_dev_no_mod)/sqrt(n);

    % Get average and SEM for MMN
    sim.MMN{drug, 1}.m_meas = mean(measured_mmn_full);
    sim.MMN{drug, 1}.SEM_meas = std(measured_mmn_full)/sqrt(n);
    sim.MMN{drug, 2}.m_meas = mean(measured_mmn_no_lat);
    sim.MMN{drug, 2}.SEM_meas = std(measured_mmn_no_lat)/sqrt(n);
    sim.MMN{drug, 3}.m_meas = mean(measured_mmn_no_mod);
    sim.MMN{drug, 3}.SEM_meas = std(measured_mmn_no_mod)/sqrt(n);

    sim.MMN{drug, 1}.m_sim = mean(simulations_mmn_full);
    sim.MMN{drug, 1}.SEM_sim = std(simulations_mmn_full)/sqrt(n);
    sim.MMN{drug, 2}.m_sim = mean(simulations_mmn_no_lat);
    sim.MMN{drug, 2}.SEM_sim = std(simulations_mmn_no_lat)/sqrt(n);
    sim.MMN{drug, 3}.m_sim = mean(simulations_mmn_no_mod);
    sim.MMN{drug, 3}.SEM_sim = std(simulations_mmn_no_mod)/sqrt(n);


end

% Initialize
explained_var.standard = zeros(3, 3);
explained_var.deviant = zeros(3, 3);
explained_var.MMN = zeros(3, 3);

% Get explained variance
for i = 1:3
    for j = 1:3
        explained_var.standard(i, j) = 1 - (std(sim.standard{i, j}.m_meas - sim.standard{i, j}.m_sim)/std(sim.standard{i, j}.m_meas))^2;
        explained_var.deviant(i, j) = 1 - (std(sim.deviant{i, j}.m_meas - sim.deviant{i, j}.m_sim)/std(sim.deviant{i, j}.m_meas))^2;
        explained_var.MMN(i, j) = 1 - (std(sim.MMN{i, j}.m_meas - sim.MMN{i, j}.m_sim)/std(sim.MMN{i, j}.m_meas))^2;
    end
end