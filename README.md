# Translational Neuromodeling Project ~ Group C 
***
Overview
========
In this project, we use dynamic causal modeling (DCM) in combination with parametric empirical bayes (PEB) to predict the drugs taken by different subjects, given MMN signals.
<br/>


Run program
===========
The user will have to run the [project_main.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/project_main.m) file in order to obtain: 
- the DCM analysis 
- the PEB analysis 
- the classification
- results in form of data and graphical schemes
<br/>

The user must have the "Statistics and Machine Learning Toolbox" installed in their MATLAB version. 
<br/> 

This project runs in MATLAB R2022 and MATLAB R2023, and with the SPM12 software.  
<br/>


### Directory layout

    tn-project                           # Main directory 
    │   
    ├── DCM                              # DCM directory
    │   ├── dcm_average_erp.m             
    │   ├── dcm_define_conditions.m       
    │   ├── dcm_no_lat_inversion.m  
    │   ├── dcm_no_mod_inversion.m 
    │   ├── dcm_full_inversion.m         
    │   ├── dcm_load_subject.m           
    │   └── dcm_main.m                   
    │ 
    ├── CV                               # Cross validation directory
    │   ├── cv_bms.m         
    │   ├── cv_concatenate.m            
    │   ├── cv_construction.m              
    │   ├── cv_group_sort.m             
    │   ├── cv_inversion_test.m         
    │   └── cv_main.m                    
    │
    ├── Metrics                          # Results directory
    │   ├── metrics_accuracy.m         
    │   ├── metrics_distance.m            
    │   ├── metrics_graphics.m              
    │   ├── metrics_main.m             
    │   ├── metrics_peb_analysis.m         
    │   └── metrics_simulations.m 
    │
    ├── utils                            # Utils directory
    │   ├── matfiles_to_array.m         
    │   └── shade.m 
    │
    ├── spm12                            # Directory for the spm code 
    │   └── ...
    │
    ├── setup_paths.m
    ├── README.md
    ├── project_main.m
    │
    └── Data                             # Directory for the data of the project 
        ├── agon                         # Directory for the data of agonists
        │   ├── DPRST_0201
        │   │   ├── DPRST_0201_MMN_preproc.dat
        │   │   └── DPRST_0201_MMN_preproc.mat
        │   └── ...
        ├── anta                         # Directory for the data of antagonists
        │   ├── DPRST_0101
        │   │   ├── DPRST_0101_MMN_preproc.dat
        │   │   ├── DPRST_0101_MMN_preproc.mat
        │   │   ├── mDPRST_0101_MMN_preprosc.dat
        │   │   └── mDPRST_0101_MMN_preproc.mat
        │   └── ...
        │
        ├── Averaged_D                   # Directory for the averaged D for each subject
        │   ├── mDPRST_0101_MMN_preproc.dat
        │   ├── mDPRST_0101_MMN_preproc.mat
        │   └── ...
        ├── FullData                     # Directory for the PEB results for each representative drug group model
        │   ├── exp_r
        │   │   ├── exp_r_drug1.mat
        │   │   └── ...
        │   ├── P_PEB
        │   │   ├── P_PEB_drug1.mat
        │   │   └── ...
        │   ├── pxp
        │   │   ├── pxp_drug1.mat
        │   │   └── ...
        │   └── ind_res_DCM.mat
        ├── exp_r                        # Directory for the exp_r calculated per drug group per CV fold 
        │   ├── exp_r_fold1_drug1.mat
        │   └── ...
        ├── P_PEB                        # Directory for the P_PEB calculated per drug group per CV fold 
        │   ├── P_PEB_fold1_drug1.mat
        │   └── ...
        ├── pxp                          # Directory for the pxp calculated per drug group per CV fold 
        │   ├── pxp_fold1_drug1.mat
        │   └── ...
        │
        ├── Full_DCM_Analysis            # Directory for the full DCMs for each subject
        │   ├── DCM_full0101.mat
        │   └── ...
        ├── Test_DCM_Analysis            # Directory for the test DCMs for each subject for each drug
        │   ├── DCM_res01_drug_1.mat
        │   └── ...
        ├── No_Lat_DCM_Analysis          # Directory for the DCMs without lateral connections for each subject
        │   ├── DCM_no_lat0101.mat
        │   └── ...
        ├── No_Mod_DCM_Analysis          # Directory for the DCMs without lateral modulations for each subject
        │   ├── DCM_no_mod0101.mat
        │   └── ...
        │
        ├── paradigm                     # Directory for the experiment setup
        │   ├── probability_structure_phases.png
        │   └── stimulus_sequence.mat
        ├── pharma                       # Directory for the subject specific drug label
        │   ├── dprst_agon_druglabels_anonym.mat
        │   ├── dprst_anta_druglabels_anonym.mat
        │   └── dprst_anta_druglabels.csv
        │
        ├── F_test.mat
        ├── ind_res_DCM.mat 
        ├── predictions.mat
        └── Information.txt
        



Description of files 
==============

- [ ] Non MATLAB files: 


filename			| description
--------------------------------|------------------------------------------
[README.md](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/README.md)			| Text file (mardown format) description of the project
[Data](https://gitlab.ethz.ch/dilamm/tn-project/-/tree/main/Data) 			| Data files for each subject (containing preprocessed EEG signals)
[dprst_anta_druglabels.csv](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/Data/pharma/dprst_anta_druglabels.csv)       | CSV file to show connection of subject number to drug label taken by the subject


- [ ] MATLAB files: 


filename			| description
--------------------------------|------------------------------------------
[project_main.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/project_main.m)		| Main script calling all necessary functions to perform the DCM analysis on the subject level, the PEB analysis on the group level, the classification and calculate the metrics and graphical representations.
[setup_paths.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/setup_paths.m)		| Adds the paths to necessary folders
[matfiles_to_array.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/utils/matfiles_to_array.m)		| Script which first reads the folder and gets its length and initializes a data array to be returned.It then iterates over the data and depending on type added reads it from the folder with the proper function. The data is then added to the array.
[shade.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/utils/shade.m)		| Fill area linear plot
||
[dcm_main.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/DCM/dcm_main.m)		| Main file for the DCM part of the project 
[dcm_average_erp.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/DCM/dcm_average_erp.m)		| Averages ERPs in all channels over the trials per condition. 
[dcm_define_conditions.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/DCM/dcm_define_conditions.m)		| Defines the conditions for trials, in terms of 'standard', 'deviant' or 'other'. 
[dcm_load_subject.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/DCM/dcm_load_subject.m)		| Loads an M/EEG file using the SPM MEEG format 
[dcm_no_lat_inversion.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/DCM/dcm_no_lat_inversion.m)		| Constructs and inverts the DCM without lateral connections for the subject 
[dcm_no_mod_inversion.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/DCM/dcm_no_mod_inversion.m)		| Constructs and inverts the DCM without lateral modulations for the subject  
[dcm_full_inversion.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/DCM/dcm_full_inversion.m)		| Constructs and inverts the full DCM for the subject 
||
[cv_main.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/CV/cv_main.m)		| Main file for the cross validation part of the project
[cv_bms.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/CV/cv_bms.m)		| Selects the DCM structure with the highest expected posterior probability and the highest protected exceedance probability
[cv_concatenate.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/CV/cv_concatenate.m)		| Concatenates 3 cell array of dimension N x 1 to form a cell array of dimension N x 3
[cv_construction.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/CV/cv_construction.m)		| Creates a cell array of DCM structures (already constructed) of Nsub subjects
[cv_group_sort.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/CV/cv_group_sort.m)		| Returns the indices of subjects in the training set that belong to the group identified by 'drug'
[cv_inversion_test.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/CV/cv_inversion_test.m)		| Inverts the DCM structures representing each drug group on a test subject, and returns the free energy corresponding to the DCM structures
||
[metrics_main.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/Metrics/metrics_main.m)		| Main file for the output of the results of the project
[metrics_distance.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/Metrics/metrics_distance.m)		| Computes the distance between empirical priors of the representative models for the drug groups calculated on the full data
[metrics_graphics.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/Metrics/metrics_graphics.m)		| Creates graphical representations of the results
[metrics_accuracy.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/Metrics/metrics_accuracy.m)		| Calculates the accuracy of the predicted drug label compared to the target drug label
[metrics_peb_analysis.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/Metrics/metrics_peb_analysis.m)		| Performs the PEB analysis on the full dataset
[metrics_simulations.m](https://gitlab.ethz.ch/dilamm/tn-project/-/blob/main/Metrics/metrics_simulations.m)		| Simulates the average of the individual subject  responses for the three model classes, in both the standard and deviant conditions and for the MMN, and compares this to the average measured responses using explained variance

***
Authors
=======
Diego LAMM, Trui OSSELAER, Lea PISTORIUS
