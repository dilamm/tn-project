% Main script calling all necessary functions to perform the DCM analysis
% on the subject level, the PEB analysis on the group level, the
% classification and calculate the metrics and graphical representations.
%_________________________________________________________________________

setup_paths()

%% DCM

% Apply DCM
dcm_main();

% This function adds all the M/EEG objects and DCM files into arrays
full_DCM = matfiles_to_array(['Data' filesep 'Full_DCM_Analysis'], "DCM"); % Full model
no_lat_DCM = matfiles_to_array(['Data' filesep 'No_Lat_DCM_Analysis'], "DCM"); % Model without lateral connections 
no_mod_DCM = matfiles_to_array(['Data' filesep 'No_Mod_DCM_Analysis'], "DCM"); % Model without lateral modulations
D = matfiles_to_array(['Data' filesep 'Averaged_D'], "MEEG");

%% CV 

cv_main(full_DCM, no_lat_DCM, no_mod_DCM, D); 

%% Figures

metrics_main(); 

