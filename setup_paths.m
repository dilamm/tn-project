function setup_paths()
% Set up paths to necessary folders.
% FORMAT dcm_eeg_channel_selection()
%__________________________________________________________________________
% 
% setup_paths adds the paths to necessary folders, including to the folders
% containing the SPM12 scripts, FieldTrip scripts, and other folders of the
% project.
%__________________________________________________________________________

% Add SPM and Fieldtrip to path
addpath(['spm12' filesep])
addpath(['spm12' filesep 'toolbox' filesep 'dcm_meeg' filesep])
addpath(['spm12' filesep 'matlabbatch' filesep])
addpath(['spm12' filesep 'external' filesep 'fieldtrip' filesep])
addpath(['spm12' filesep 'external' filesep 'fieldtrip' filesep 'forward' filesep])
addpath(['spm12' filesep 'external' filesep 'fieldtrip' filesep 'utilities' filesep])
addpath(['spm12' filesep 'external' filesep 'fieldtrip' filesep 'fileio' filesep])
addpath(['spm12' filesep 'external' filesep 'fieldtrip' filesep 'plotting' filesep])
addpath(['spm12' filesep 'toolbox' filesep 'Neural_Models' filesep])

% Add DCM\ folder to path
addpath(['DCM' filesep])

% Add CV\ folder to path
addpath(['CV' filesep])

% Add Metrics\ folder to path
addpath(['Metrics' filesep])

% Add utils\ folder to path
addpath(['utils' filesep])

end