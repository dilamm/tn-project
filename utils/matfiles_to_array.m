function data = matfiles_to_array(folder_path, type)
% Read all .mat files in the folder and add it into an array.
% FORMAT data = matfiles_to_array(folder_path, type)
%
% folder path        - The folder path of the data that wants to be put
%                      added into an array
% type               - The type of the data that is in the folder (required
%                      because different types of data require different
%                      functions to read out
%
% data               - The array full of the data
%__________________________________________________________________________
%
% matfiles_to_array first reads the folder and gets its length and
% initializes a data array to be returned.It then iterates over the data
% and depending on type added reads it from the folder with the proper
% function. The data is then added to the array.
%__________________________________________________________________________

% Read the folder, get its length, initiate return array
mat_files = dir(fullfile(folder_path, '*.mat'));
num_files = length(mat_files);
data = cell(num_files, 1);

% Iterate over the .mat files in the folder and add them to an array
for i = 1:num_files
    file_path = fullfile(mat_files(i).folder, mat_files(i).name);

    if type == "MEEG"
        mat_data = spm_eeg_load(file_path);

    elseif type == "DCM"
        mat_data = load(file_path);
        mat_data = mat_data.DCM;

    else
        mat_data = load(file_path);

    end

    data{i} = mat_data;

end